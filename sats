#!/usr/bin/perl
use strict;
use warnings;
use Audio::MPD q{0.19.0};
use Getopt::Long;

=head1 NAME

sats - mpd stop after this song

=head1 SYNOPSIS

sats [-d] [-n num] [host]

=head1 DESCRIPTION

B<sats> is an acronym for Stop After This Song. It will wait for playback
of the currently playing song to finish, and then tell mpd to stop playing.

If the hostname is omitted, the MPD_HOST environment variable will be used.

=head1 OPTIONS

=over 4

=item -d

Daemonize rather than waiting in the foreground for the song to stop playing.

=item -n num

Stop after B<num> songs (default is 1).

=back

=head1 AUTHOR

Copyright 2007 Joey Hess <joey@kitenet.net>

Licensed under the GNU GPL version 2 or higher.

http://kitenet.net/~joey/code/mpdtoys

=cut

my $num=1;
my $daemon=0;
GetOptions(
	"n=i" => \$num,
	"d" => \$daemon,
) || usage();

sub usage {
	die "Usage: sats [-d] [-n num] [host]\n";
}

if (@ARGV) {
	$ENV{MPD_HOST}=shift;
}

my $mpd=Audio::MPD->new(conntype => "reuse");

my $song=$mpd->current;
if ($mpd->status->state ne 'play') {
	die "no song is currently playing\n";
}

if ($daemon) {
	eval q{use Proc::Daemon};
	Proc::Daemon::Init();
	# daemonising closed the connection to mpd
	$mpd=Audio::MPD->new(conntype => "reuse");
}

# Polling is evil, it could look at the seek position, and sleep until the
# end. But that would break if something seeked in the song..

while (sleep 1) {
	last if $mpd->status->state ne 'play';
	my $current=$mpd->current;
	if ($current->id != $song->id) {
		$num--;
		if ($num == 0) {
			$mpd->stop;
			exit;
		}
		$song=$current;
	}
}
