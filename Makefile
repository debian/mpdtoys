progs=mpstore mprev mpgenplaylists mpfade sats mpskip mptoggle mprand \
      mpinsert vipl mprompt mplength mprandomwalk

PERLLIBDIR=$(shell perl -e 'use Config; print $$Config{installvendorlib}')

build:

install:
	install -d $(DESTDIR)/usr/bin/
	install $(progs) $(DESTDIR)/usr/bin/
	
	install -d $(DESTDIR)/usr/share/man/man1/
	for prog in $(progs); do \
		pod2man -c $$prog $$prog > $(DESTDIR)/usr/share/man/man1/$$prog.1; \
	done
	
	for link in mpload mpcp mpmv mpswap; do \
		ln -sf mpstore $(DESTDIR)/usr/bin/$$link; \
		ln -sf mpstore.1 $(DESTDIR)/usr/share/man/man1/$$link.1; \
	done

	install -d $(DESTDIR)/$(PERLLIBDIR)
	install -m 0644 MpdToys.pm $(DESTDIR)/$(PERLLIBDIR)
