#!/usr/bin/perl
use strict;
use warnings;
use Audio::MPD q{0.19.0};

=head1 NAME

mprandomwalk - play random bits of all queued songs

=head1 SYNOPSIS

mprandomwalk [maxduration] [host]

=head1 DESCRIPTION

B<mprandomwalk> plays random segments of randomly chosen songs in the
playlist.

The B<maxduration> can control the maximum number of seconds of a song to
play at a time.

If the hostname is omitted, the MPD_HOST environment variable will be used.

=head1 AUTHOR

Copyright 2010 Joey Hess <joey@kitenet.net>

Licensed under the GNU GPL version 2 or higher.

http://kitenet.net/~joey/code/mpdtoys

=cut

my $maxduration=10;
if (@ARGV && $ARGV[0] =~ /^[0-9.]+$/) {
	$maxduration=shift;
}

if (@ARGV) {
	$ENV{MPD_HOST}=shift;
}

my $mpd=Audio::MPD->new(conntype => "reuse");

$mpd->play;

my $pl=$mpd->playlist;
my @list=$pl->as_items;

if (! @list) {
	die "error: no songs queued\n";
}

while (1) {
	my $song=$list[rand($#list)];
	my $length=$song->time;
	next unless $length;
	my $duration=rand($maxduration);
	my $pos=rand($length-$duration);
	$mpd->seekid($pos, $song->id);
	sleep $duration;
}
